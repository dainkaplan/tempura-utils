name := "common-utils"

fork in run := true

version := "0.1-SNAPSHOT"

organization := "org.tempura"

scalaVersion := "2.10.4"

resolvers ++= Seq("Sonatype Nexus releases" at "https://oss.sonatype.org/content/repositories/releases", 
	"Sonatype Nexus snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
	"Central repository" at "http://repo1.maven.org/maven2",
	"Scala-Tools repo" at "http://scala-tools.org/repo-releases/",
	"ScalaNLP Maven2" at "http://repo.scalanlp.org/repo",
	"T repo" at "http://maven.twttr.com/")

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "spray" at "http://repo.spray.io/"

libraryDependencies ++= Seq(
	"org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
	"com.novocode" % "junit-interface" % "0.7" % "test->default")
	
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.0"

libraryDependencies += "com.twitter" % "util-eval_2.10" % "6.3.4"	
	
libraryDependencies += "com.github.scala-incubator.io" % "scala-io-file_2.10" % "0.4.2"

//libraryDependencies += "org.scalala" %% "scalala" % "1.0.0.RC3-SNAPSHOT"

//libraryDependencies ++= Seq("org.scalanlp" %% "breeze-viz" % "0.1", "org.scalanlp" %% "breeze-math" % "0.1")

libraryDependencies += "io.spray" %%  "spray-json" % "1.2.4" //cross CrossVersion.full

libraryDependencies += "com.fasterxml.jackson.module" % "jackson-module-scala_2.10" % "2.1.3"

//EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource
