package org.tempura.utils.primitives

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers

import org.tempura.utils.primitives

@RunWith(classOf[JUnitRunner])
class PrimitivesTests extends FunSuite with ShouldMatchers {

  test("can properly map boxed/unboxed primitives to corresponding types") {
    assert(primitives.is(classOf[Int] -> classOf[java.lang.Long]) === false)
    assert(primitives.is(classOf[Int] -> classOf[java.lang.Integer]) === true)
    assert(primitives.is(classOf[Int] -> classOf[Int]) === true)
    assert(primitives.is(classOf[java.lang.Character] -> classOf[Char]) === true)
    assert(primitives.is(classOf[java.lang.Character] -> classOf[Byte]) === false)
    assert(primitives.is(classOf[Byte] -> classOf[java.lang.Character]) === false)
  }

  test("can ensure unboxed Class[_]es") {
    assert(primitives.ensureUnboxed(classOf[java.lang.Integer]) === classOf[Int])
    assert(primitives.ensureUnboxed(classOf[Int]) === classOf[Int])
  }
}