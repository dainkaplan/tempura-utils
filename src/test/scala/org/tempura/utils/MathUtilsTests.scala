package org.tempura.utils

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers

@RunWith(classOf[JUnitRunner])
class MathUtilsTests extends FunSuite with ShouldMatchers {

  import MathOps._

  test("Can properly argmax") {
    def f(x: Int) = x * -1
    val c = (-10 to 0)
    assert(c.argmax(f).toSet === Set(-10))
    // or alternate calling syntax
    assert(argmax(c)(f).toSet === Set(-10))
  }

  test("Can properly argmax with multiple maxes") {
    assert((-10 to 10).argmax(math.pow(_, 2)).toSet === Set(-10, 10))
  }

  test("Can properly add elements from two lists to make a third list") {
    val added = new AddTwoIterables(List(1, 2, 3)) +++ List(3, 2, 1)
    assert(added === List(4, 4, 4))
  }

  test("Can properly take the dot product of two vectors") {
    val a = List(0.5, 0.5, 0.5)
    val b = List(0.5, 0, 0)
    locally {
      val dot = a dot b
      assert(dot === 0.25)
    }
    locally {
      val dot = (a zip b).dotProduct
      assert(dot === 0.25)
    }
  }
}