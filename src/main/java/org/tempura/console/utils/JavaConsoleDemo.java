package org.tempura.console.utils;

import static org.tempura.console.utils.Ansi.*;
import org.tempura.console.utils.ConsoleWriter;

public class JavaConsoleDemo {
    private final ConsoleWriter screen;
    public JavaConsoleDemo(ConsoleWriter screen) {
        this.screen = screen;
    }

    public void rewriteWithColors(String name) throws Exception  {
        screen.clearAll();
        screen
                .print(Red().and(BgYellow()).format("Hello"))
                .print(" ")
                .print(Yellow().format("World"));
        screen.display();

        Thread.sleep(1000);

        screen.clearAll();
        screen
                .print(Blue().and(BgWhite()).format("Hello"))
                .print(" ")
                .print(Magenta().and(BgBlue()).format("%s", name));
        screen.display();

        Thread.sleep(1000);
    }

    public void rewrite(String name) throws Exception {
        screen.clearAll();
        screen.print("Hello\nWorld");
        screen.display();

        Thread.sleep(1000);

        screen.clearAll();
        screen.print("Hello\n" + name);
        screen.display();

        Thread.sleep(1000);
    }

    public static void main(final String[] args) throws Exception {
        ConsoleWriter screen = new ConsoleWriter(System.out);
        JavaConsoleDemo demo = new JavaConsoleDemo(screen);
        demo.rewrite(args[0]);
        demo.rewriteWithColors(args[0]);
    }
}
