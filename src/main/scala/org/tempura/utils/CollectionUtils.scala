package org.tempura.utils

import scala.collection.GenIterableLike
import collection.mutable.{Map => MMap}

object CollectionOps {

  import scala.collection.parallel.ForkJoinTaskSupport
  import scala.collection.parallel.ParSeq
  import scala.collection.parallel.TaskSupport
  import scala.collection.IterableLike

  implicit class ParOps[A](seq: ParSeq[A]) {
    def withTS(implicit task: TaskSupport) = {
      seq.tasksupport = task
      seq
    }

    def withThreads(num: Int) = {
      import scala.concurrent.forkjoin.ForkJoinPool
      val n = if (num < 1) {
        Runtime.getRuntime.availableProcessors
      } else num
      val task = new ForkJoinTaskSupport(new ForkJoinPool(n))
      seq.tasksupport = task
      seq
    }
  }

  implicit class TupleToList[T <: Product](t: T) {
    def toListOf[A] = t.productIterator.toList.asInstanceOf[List[A]]

    // Must specify type!
    def toList = t.productIterator.toList

    //  class Test[T](implicit m : Manifest[T]) {
    //   val testVal = m.erasure.newInstance().asInstanceOf[T]
    // }
  }

  implicit class CollectionOps[+A, B <: Iterable[A]](col: GenIterableLike[A, B]) {
    def findFirst[B](f: A => Option[B]): Option[B] = {
      col.repr.view.map(f).collectFirst({
        case Some(x) => x
      })
    }

    def groupByKey[B, C](implicit ev: A <:< (B, C)) = {
      col.groupBy(x => x._1).map(x => (x._1 -> x._2.unzip._2.toList)).seq.toMap
    }

    def takeUntil(predicate: A => Boolean, inclusive: Boolean = false) = {
      col.zipWithIndex.find(x => predicate(x._1)) match {
        case Some((a, idx)) => col.take(idx + (if (inclusive) 1 else 0))
        case _              => col.repr
      }
    }

    def takeWhile2(predicate: A => Boolean, inclusive: Boolean = false): B = {
      col.zipWithIndex.find(x => !predicate(x._1)) match {
        case Some((a, idx)) => col.take(idx + (if (inclusive) 1 else 0))
        case _              => col.repr
      }
    }

    def zipWithIndexFrom(startIndex: Int) = {
      col.zipWithIndex.map(x => (x._1, x._2 + startIndex))
    }

    def indexesWhere(op: A ⇒ Boolean) = {
      col.foldLeft(List[Int]() -> 0) {
        (ret, c) =>
          if (op(c))
            (ret._1 :+ ret._2) -> (ret._2 + 1)
          else
            ret._1 -> (ret._2 + 1)
      }._1
    }
  }

  implicit class ListOps[A](col: List[A]) {
    def removeIndex(idx: Int) = {
      (col zipWithIndex).filter(_._2 != idx).unzip._1
    }

    // This was removed in 2.10, so we add it back here.
    def --[B >: A](that: List[B]): List[B] = {
      col.filterNot(that.contains)
//      val b = new collection.mutable.ListBuffer[B]
//      var these = this
//      while (!these.isEmpty) {
//        if (!that.contains(these.head)) b += these.head
//        these = these.tail
//      }
//      b.toList
    }
  }

  implicit class UnzipOps[A <: Product](it: Iterable[A]) {
    // unzip4
    def unzip4[B, C, D, E](implicit asQuadruple: A <:< (B, C, D, E)) = {
      it.foldLeft((List[B](), List[C](), List[D](), List[E]())) {
        (list, i) =>
          (
            list._1 :+ i._1,
            list._2 :+ i._2,
            list._3 :+ i._3,
            list._4 :+ i._4)
      }
    }

    // unzip5
    def unzip5[B, C, D, E, F](implicit asQuintuple: A <:< (B, C, D, E, F)) = {
      it.foldLeft((List[B](), List[C](), List[D](), List[E](), List[F]())) {
        (list, i) =>
          (
            list._1 :+ i._1,
            list._2 :+ i._2,
            list._3 :+ i._3,
            list._4 :+ i._4,
            list._5 :+ i._5)
      }
    }
  }

  implicit class ZipOps[+A, +Repr](repr: GenIterableLike[A, Repr]) {

    import scala.collection.GenIterable
    import scala.collection.generic.CanBuildFrom

    def zip3[B, C, That](b: GenIterable[B], c: GenIterable[C])(implicit bf: CanBuildFrom[Repr, (A, B, C), That]): That = {
      val res = bf(repr.repr)
      val ait = repr.iterator
      val bit = b.iterator
      val cit = c.iterator
      while (ait.hasNext && bit.hasNext && cit.hasNext)
        res += ((ait.next(), bit.next(), cit.next()))
      res.result()
    }

    def zip4[B, C, D, That](b: GenIterable[B], c: GenIterable[C], d: GenIterable[D])(implicit bf: CanBuildFrom[Repr, (A, B, C, D), That]): That = {
      val res = bf(repr.repr)
      val ait = repr.iterator
      val bit = b.iterator
      val cit = c.iterator
      val dit = d.iterator
      while (ait.hasNext && bit.hasNext && cit.hasNext && dit.hasNext)
        res += ((ait.next(), bit.next(), cit.next(), dit.next()))
      res.result()
    }

    def zip5[B, C, D, E, That](b: GenIterable[B], c: GenIterable[C], d: GenIterable[D], e: GenIterable[E])(implicit bf: CanBuildFrom[Repr, (A, B, C, D, E), That]): That = {
      val res = bf(repr.repr)
      val ait = repr.iterator
      val bit = b.iterator
      val cit = c.iterator
      val dit = d.iterator
      val eit = e.iterator
      while (ait.hasNext && bit.hasNext && cit.hasNext && dit.hasNext && eit.hasNext)
        res += ((ait.next(), bit.next(), cit.next(), dit.next(), eit.next()))
      res.result()
    }
  }

  implicit class SetOps[A](set: Set[A]) {
    def ^(that: Set[A]) = {
      // Union minus intersection is exclusive disjunction.
      (set | that) -- (set & that)
    }

    def xor = ^ _
  }

  implicit class MapOfMaps[A, B, C](m: MMap[A, MMap[B, C]]) {
    // TODO: Make this generic and recursively apply to all nested collections.
    def immutify = m.map(x => (x._1, x._2.toMap)).toMap
  }

  implicit class MMapOps[A, B](col: MMap[A, B]) {

    // NOTE: If the map doesn't have a defaultSet (using withDefault) this will not work!
    def ?(a: A) = col.getOrElseUpdate(a, col(a))

    def modify(k: A)(f: B => B) = {
      col.update(k, f(col(k)))
    }

    // map.modify("key") { _ + 1 }
  }

  implicit class MapOps[A, B](col: Map[A, B]) {
    def valuesAsSet[C](implicit ev: B <:< Iterable[C]) = {
      col.map(x => (x._1, x._2.toSet))
    }

    def valuesAsList[C](implicit ev: B <:< Iterable[C]) = {
      col.map(x => (x._1, x._2.toList))
    }
  }

}

object MergeMaps {

  implicit class MergeMaps[A, B](ma: Map[A, B]) {
    def merge(mb: Map[A, B])(merger: (B, B) => B): Map[A, B] =
      (Map[A, B]() /: (for (m <- List(ma, mb); kv <- m) yield kv)) {
        (map, kv) =>
          map + (if (map.contains(kv._1)) kv._1 -> merger(map(kv._1), kv._2) else kv)
      }
  }

}

object NumericMergeMaps {

  import MergeMaps._

  implicit class NumericMergeMaps[A, B](ma: Map[A, B])(implicit numeric: Numeric[B]) {
    def merge(mb: Map[A, B]): Map[A, B] = {
      new MergeMaps[A, B](ma).merge(mb)((a, b) => numeric.plus(a, b))
    }
  }

}
