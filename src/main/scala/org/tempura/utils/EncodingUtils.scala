package org.tempura.utils

import scalax.io.Codec.charset2codec
import scalax.io.JavaConverters.asInputConverter
import scalax.io.JavaConverters.asOutputConverter

object EncodingUtils {

  /**
   * This method ensures that the output String has only
   * valid XML unicode characters as specified by the
   * XML 1.0 standard. For reference, please see
   * <a href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the
   * standard</a>. This method will return an empty
   * String if the input is null or empty.
   *
   * NOTE: Original java implementation found at: http://blog.mark-mclaren.info/2007/02/invalid-xml-characters-when-valid-utf8_5873.html
   *
   * @param in The String whose non-valid characters we want to remove.
   * @return The in String, stripped of non-valid characters.
   */
  def stripNonValidXMLCharacters(in: String): String = {
    val out = new StringBuffer(); // Used to hold the output.
    var current: Char = 0; // Used to reference the current character.

    if (in == null || ("".equals(in))) return ""; // vacancy test.
    for (i <- 0 until in.length()) {
      current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
      if ((current == 0x9) ||
        (current == 0xA) ||
        (current == 0xD) ||
        ((current >= 0x20) && (current <= 0xD7FF)) ||
        ((current >= 0xE000) && (current <= 0xFFFD)) ||
        ((current >= 0x10000) && (current <= 0x10FFFF)))
        out.append(current);
    }
    return out.toString();
  }
  def stripNonValidXMLCharacters(infile: java.io.File) {
    import scalax.io.JavaConverters._
    val data = infile.asInput.string
    infile.renameTo(new java.io.File(infile.getAbsolutePath + ".bak"))
    infile.asOutput.write(stripNonValidXMLCharacters(data))(scalax.io.Codec.UTF8)
  }
}