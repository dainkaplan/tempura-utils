package org.tempura.utils

object ClosableOps {
  // Supplying foreach here will autolift any type with close()
  // to this implicit class, but to avoid ambiguity, we also
  // create an conversion method, asClosable, to mark this lift.
  implicit class Closable[T <: { def close() }](closable: T) {
    def closeAfter(run: T => Unit) {
      run(closable)
      closable.close()
    }
    def foreach(run: T => Unit) {
      closeAfter(run)
    }
  }
  implicit class AsClosable[T <: { def close() }](closable: T) {
    def asClosable = new Closable(closable)
  }

  // E.g.:
  // for (stream <- new PrintStream().lastly(_.close())) { }
  implicit class AsCallAfter[T <: AnyRef](obj: T) {
    def lastly(after: T => Unit) = new CallAfter(obj, after(obj))
  }
  class CallAfter[T <: AnyRef](obj: T, f: => Unit) {
    def foreach(run: T => Unit) {
      run(obj)
      f
    }
  }
}

object FileIO {
  import java.io.File

  final val classpathMarker = "classpath:"

  /**
   * Will look in classpath IFF the string is prepended with "classpath:", or disk otherwise.
   */
  def getFile(filepath: String) = {
    val filepath_ =
      if (filepath.startsWith(classpathMarker))
        this.getClass().getResource(filepath.substring(classpathMarker.length)).getFile
      else
        filepath
    new File(filepath_)
  }

  /**
   * Optionally returns a File from classpath or disk by the specified ordering of search.
   */
  def getFile(filepath: String, classpathFirst: Boolean = true): Option[File] = {
    def getFromClasspath = {
      val url = this.getClass().getResource(filepath)
      if (url != null) Some(new File(url.getFile)) else None
    }
    def getFromDirectory = {
      val f = new File(filepath)
      if (f.exists) Some(f) else None
    }
    if (classpathFirst)
      getFromClasspath orElse getFromDirectory
    else
      getFromDirectory orElse getFromClasspath
  }

  implicit class FilenameSafeString(str: String) {
    // TODO: There are other bad chars too, like ^ chars.
    val macBadChars = ":/".toSet
    val unixBadChars = "/".toSet
    val winBadChars =  "/?<>\\:*|\"".toSet
    val badChars = macBadChars ++ unixBadChars ++ winBadChars

    def fileNameSafe = str
      .map(c => if (badChar(c)) escape(c) else c.toString)
      .mkString

    def escape(c: Char) = scala.reflect.NameTransformer.encode(c.toString)
    def badChar(c: Char) = badChars.contains(c)
  }

  // Basically taken from:
  // http://stackoverflow.com/questions/204784/how-to-construct-a-relative-path-in-java-from-two-absolute-paths-or-urls
  def relativeFilePath(file: File, base: File) = {
    base.toURI.relativize(file.toURI).getPath
  }

  implicit class RelativeFilePath(f: File) {
    def relativePath(base: File) = relativeFilePath(f, base)
  }
}

object ObjectIO {
  import java.io.ObjectInputStream
  import java.io.ObjectOutputStream
  import java.io.FileInputStream
  import java.io.FileOutputStream
  def readObjectFromFile[A](filename: String)(implicit m: scala.reflect.Manifest[A]): A = {
    val input = new ObjectInputStream(new FileInputStream(filename))
    val obj = input.readObject()
    input.close
    obj match {
      case x if m.erasure.isInstance(x) => x.asInstanceOf[A]
      case x => sys.error(
        "Type %s not what was expected (%s) when reading from file".
          format(x.getClass.getSimpleName, m.erasure.getSimpleName))
    }
  }
  def writeObjectToFile(obj: AnyRef)(filename: String) {
    val output = new ObjectOutputStream(new FileOutputStream(filename))
    output.writeObject(obj);
    output.close();
  }
}

object JsonIO {
  object Spray {
    import spray.json._
    import DefaultJsonProtocol._ // !!! IMPORTANT, else `convertTo` and `toJson` won't work
    def readObjectFromFile[A](filename: String)(implicit format: spray.json.JsonFormat[A]): A = {
      val in = scala.io.Source.fromFile(filename).getLines.mkString("\n")
      SprayWrapper.deserialize(in)(format)
    }
    def writeObjectToFile[A](obj: A)(filename: String)(implicit format: spray.json.JsonFormat[A]) {
      val out = new java.io.PrintWriter(filename)
      out.print(SprayWrapper.serialize(obj)(format))
      out.close()
    }
  }

  object Jackson {
    import com.fasterxml.jackson.databind.ObjectMapper
    def readObjectFromFile[A](filename: String)(implicit ev: Manifest[A], mapper: ObjectMapper = JacksonWrapper.defaultMapper): A = {
      JacksonWrapper.deserializeFromFile[A](filename)
    }
    def writeObjectToFile[A](obj: A)(filename: String)(implicit mapper: ObjectMapper = JacksonWrapper.defaultMapper) {
      JacksonWrapper.serializeToFile(obj)(filename)
    }
  }
}