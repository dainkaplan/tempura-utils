package org.tempura.utils

class MetaDataToStringMap(attr: xml.MetaData) {
  def toStringMap = attr.map(a => (a.key, a.value.mkString(""))).toMap
  def toStringMapOpt = if (attr.length > 0) Some(toStringMap) else None
}

class AddAttributeMapToElem(elem:xml.Elem) {
  def addAttributes(attrs: Map[String,String]) = {
    attrs.foldLeft(elem)((tag, attr) => (tag % xml.Attribute(None, attr._1, xml.Text(attr._2), xml.Null)))
  }
}

object XmlOps {
  implicit def toMetaDataToMap(attr: xml.MetaData) = new MetaDataToStringMap(attr)
  implicit def toAddAttributeMapToElem(elem: xml.Elem) = new AddAttributeMapToElem(elem)
}