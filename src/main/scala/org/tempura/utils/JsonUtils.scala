package org.tempura.utils

// Taken largely from:
// http://stackoverflow.com/questions/12591457/scala-2-10-json-serialization-and-deserialization
//
object JacksonWrapper {
  import java.lang.reflect.{ Type, ParameterizedType }
  import com.fasterxml.jackson.databind.ObjectMapper
  import com.fasterxml.jackson.module.scala.DefaultScalaModule
  import com.fasterxml.jackson.annotation.JsonProperty;
  import com.fasterxml.jackson.core.`type`.TypeReference;

  lazy val defaultMapper: ObjectMapper = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper
  }

  def serialize(value: Any)(implicit mapper: ObjectMapper = defaultMapper): String = {
    import java.io.StringWriter
    val writer = new StringWriter()
    mapper.writeValue(writer, value)
    writer.toString
  }

  def serializeToFile(value: Any)(path: String)(implicit mapper: ObjectMapper = defaultMapper) {
    mapper.writeValue(new java.io.File(path), value)
  }

  def deserialize[T](value: String)(implicit ev: Manifest[T], mapper: ObjectMapper = defaultMapper): T =
    mapper.readValue(value, typeReference[T])

  def deserializeFromFile[T](path: String)(implicit ev: Manifest[T], mapper: ObjectMapper = defaultMapper): T = {
    val r = scala.io.Source.fromFile(path)
    mapper.readValue(r.bufferedReader, typeReference[T])
  }

  private[this] def typeReference[T: Manifest] = new TypeReference[T] {
    override def getType = typeFromManifest(manifest[T])
  }

  private[this] def typeFromManifest(m: Manifest[_]): Type = {
    if (m.typeArguments.isEmpty) { m.erasure }
    else new ParameterizedType {
      def getRawType = m.erasure
      def getActualTypeArguments = m.typeArguments.map(typeFromManifest).toArray
      def getOwnerType = null
    }
  }
}

// Requires an implicit JsonFormat for the types in question.
object SprayWrapper {
  import spray.json._
  import DefaultJsonProtocol._ // !!! IMPORTANT, else `convertTo` and `toJson` won't work
  def serialize[A](obj: A)(implicit format: spray.json.JsonFormat[A]): String = {
    val jsonAst = obj.toJson(format)
    jsonAst.compactPrint
  }
  def deserialize[A](in: String)(implicit format: spray.json.JsonFormat[A]): A = {
    val jsonAst = in.asJson
    val obj = jsonAst.convertTo[A](format)
    obj
  }
}