package org.tempura.utils

object Timed {
  import java.util.concurrent.TimeUnit

  def timed[T](msg: String)(block: => T) = {
    val start = System.nanoTime
    val res = block
    val end = System.nanoTime
    val elapsed = TimeUnit.MILLISECONDS.convert(end - start, TimeUnit.NANOSECONDS)
    println("%s: Took %.3f seconds".format(msg, elapsed.toDouble / 1000))
    res
  }

  def timed[T](block: => T) = {
    val start = System.nanoTime
    val res = block
    val end = System.nanoTime
    val elapsed = TimeUnit.MILLISECONDS.convert(end - start, TimeUnit.NANOSECONDS)
    println("Took %.3f seconds".format(elapsed.toDouble / 1000))
    res
  }
}
