package org.tempura.utils

import scala.util.matching.Regex

object StringOps {

  implicit class StringComp(str: String) {
    def ==~(that: String) = str equalsIgnoreCase that

    def !=~(that: String) = !(==~(that))

    def =~(that: String) = (that.r findFirstIn str).isDefined

    def =~(that: Regex) = (that findFirstIn str).isDefined

    def !~(that: String) = !(=~(that))

    def !~(that: Regex) = !(=~(that))
  }

  implicit class TrimString(str: String) {
    def trimRight(toChop: Int) = str.dropRight(toChop)

    def chop = trimRight(1)

    def -(toChop: Int) = trimRight(toChop)
  }

  implicit class StringWrap(str: String) {
    def wrap(wrap: String) = wrap + str + wrap
  }

  implicit class CountSubstrings(str: String) {
    def substringCount(find: String): Int = {
      import scala.annotation.tailrec
      @tailrec def count__(pos: Int, c: Int): Int = {
        val idx = str.indexOf(find, pos)
        if (idx == -1) c
        else count__(idx + find.length, c + 1)
      }
      count__(0, 0)
    }
  }

}

