package org.tempura.utils
import java.io.File
import com.twitter.util.Config

/*
 * Usage is simple:
 * 
 * class MyConfig extends SimpleConfig { 
 *     var field1 = required[String]
 *     var field2 = optional[String]
 * }
 * val config = SimpleConfig.getValidated[MyConfig](configFileName)
 * doSomethingWithField1(config.field1)
 * 
 * */

// Hack to not have to specify a wrapping class
class SimpleConfig extends Config[String] {
	def apply = getClass.getSimpleName
}

object SimpleConfig {

	def getValidated[T <: SimpleConfig](fileName: String) = {
		val file = new File(fileName)
		val eval = new com.twitter.util.Eval()
		val config = eval[T](file)
		config.validate()
		config
	}
}