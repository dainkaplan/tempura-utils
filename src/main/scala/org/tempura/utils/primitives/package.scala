package org.tempura.utils

package object primitives {
  object boxed {
    val intM = ClassManifest.fromClass(classOf[java.lang.Integer])
    val doubleM = ClassManifest.fromClass(classOf[java.lang.Double])
    val longM = ClassManifest.fromClass(classOf[java.lang.Long])
    val floatM = ClassManifest.fromClass(classOf[java.lang.Float])
    val booleanM = ClassManifest.fromClass(classOf[java.lang.Boolean])
    val charM = ClassManifest.fromClass(classOf[java.lang.Character])
    val byteM = ClassManifest.fromClass(classOf[java.lang.Byte])
    val shortM = ClassManifest.fromClass(classOf[java.lang.Short])
  }

  object unboxed {
    val intM = manifest[Int] // java.lang.Integer.TYPE
    val doubleM = manifest[Double] // java.lang.Double.TYPE
    val longM = manifest[Long] // java.lang.Long.TYPE
    val floatM = manifest[Float] // java.lang.Float.TYPE
    val booleanM = manifest[Boolean] // java.lang.Boolean.TYPE
    val charM = manifest[Char] // java.lang.Character.TYPE
    val byteM = manifest[Byte] // java.lang.Byte.TYPE
    val shortM = manifest[Short] // java.lang.Short.TYPE
  }

  val anyValM = manifest[AnyVal]
  val anyRefM = manifest[AnyRef]

  private val fromUnboxed = Map[ClassManifest[_], ClassManifest[_]](
    unboxed.intM -> boxed.intM,
    unboxed.doubleM -> boxed.doubleM,
    unboxed.longM -> boxed.longM,
    unboxed.floatM -> boxed.floatM,
    unboxed.booleanM -> boxed.booleanM,
    unboxed.charM -> boxed.charM,
    unboxed.byteM -> boxed.byteM,
    unboxed.shortM -> boxed.shortM)

  private val fromBoxed = Map[ClassManifest[_], ClassManifest[_]](
    boxed.intM -> unboxed.intM,
    boxed.doubleM -> unboxed.doubleM,
    boxed.longM -> unboxed.longM,
    boxed.floatM -> unboxed.floatM,
    boxed.booleanM -> unboxed.booleanM,
    boxed.charM -> unboxed.charM,
    boxed.byteM -> unboxed.byteM,
    boxed.shortM -> unboxed.shortM)

  private val map = fromUnboxed ++ fromBoxed

  def ensureUnboxed(clazz: Class[_]) = {
    val clazzM = ClassManifest.fromClass(clazz)
    fromBoxed.getOrElse(clazzM, clazzM).erasure
  }

  def is(classes: (Class[_], Class[_])): Boolean = is(classes._1, classes._2)

  def is(clazz: Class[_], isType: Class[_]): Boolean = {
    val isTypeM = ClassManifest.fromClass(isType)
    val clazzM = ClassManifest.fromClass(clazz)

    (map contains isTypeM) && (map contains clazzM) &&
      (clazzM == isTypeM || map(clazzM) == isTypeM)
  }
}