package org.tempura.utils


object ThreadLocalOps {

  implicit class ThreadLocalOps[T](tl: ThreadLocal[T]) {
    def getOrElseUpdate(update: => T): T = {
      val v = tl.get
      if (v == null) {
        val v2 = update
        tl set v2
        v2
      } else v
    }
  }

}

object BasicOps {

  // Does not  get parent class fields! Should be used for simple case classes only.
  implicit class ParamsToMap(obj: AnyRef) extends ClassOps {

    import org.tempura.utils.ClassOps.PrettyClassName

    def fieldMap =
      (collection.immutable.ListMap[String, Any]() /: obj.getClass.getDeclaredFields) {
        (a, f) =>
          f.setAccessible(true)
          a + (f.getName -> f.get(obj))
      }

    def toPrettyString = asString(fieldNames = true)

    // asString(newlines=true, indent="  ", maxDepth=1)
    def asString(fieldNames: Boolean = true, ignore: List[String] = List(),
                 newlines: Boolean = false,
                 indent: String = "  ", maxDepth: Int = 1): String = {
      def maybeName(n: String) = {
        if (fieldNames)
          n + "="
        else ""
      }
      def prettyValue(v: Any) = {
        v match {
          case vv: String => "\"" + vv + "\""
          case vv         => vv
        }
      }
      prettyClassName + "(" + fieldMap.filterNot(ignore.contains).map(x => maybeName(x._1) + prettyValue(x._2)).mkString(", ") + ")"
    }

    def prettyClassName = new PrettyClassName(obj).prettyClassName
  }

}

trait ClassOps {
  def makePrettyClassName(cls: Class[_]) = {
    scala.reflect.NameTransformer.decode(cls.getSimpleName)
  }

  def encodedClassName(name: String) = {
    scala.reflect.NameTransformer.encode(name)
  }
}

object ClassOps extends ClassOps {

  case class `S_{test}Class`()

  implicit class PrettyClassName[C](obj: C) {
    def prettyClassName = obj match {
      case cls: Class[_] => makePrettyClassName(cls)
      case _             => makePrettyClassName(obj.getClass)
    }
  }

}
