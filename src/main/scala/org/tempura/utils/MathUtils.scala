package org.tempura.utils

object Divergence {
  // kl and js divergence ported from Mallet (cc.mallet.util.Maths)
  private val log2 = math.log(2)

  def klDivergence(p1: Array[Double], p2: Array[Double]): Double = {
    assert(p1.length == p2.length)
    //    var klDiv = 0d
    //    for {
    //      i <- 0 until p1.length
    //      if !(p1(i) == 0)
    //    } {
    //      if (p2(i) == 0) return Double.PositiveInfinity
    //      klDiv += p1(i) * math.log(p1(i) / p2(i))
    //    }
    //    klDiv / log2

    val klDiv = (0 until p1.length).
      filterNot(p1(_) == 0).
      foldLeft(0d) {
      (klDiv, i) =>
        if (p2(i) == 0) return Double.PositiveInfinity
        klDiv + p1(i) * math.log(p1(i) / p2(i))
    }
    klDiv / log2
  }

  def jsDivergence(p1: Array[Double], p2: Array[Double]): Double = {
    assert(p1.length == p2.length)
    val average = for {
      i <- (0 until p1.length).toArray
    } yield (p1(i) + p2(i)) / 2
    (klDivergence(p1, average) + klDivergence(p2, average)) / 2
  }

  def alphaSkew(p1: Array[Double], p2: Array[Double], alpha: Double = 0.99): Double = {
    val div = (0 until p1.length).
      filterNot(p1(_) == 0).
      foldLeft(0d) {
      (div, i) =>
        div + p1(i) * math.log(p1(i) / (alpha * p2(i)) + (((1 - alpha) * p1(1))))
    }
    div / log2
  }
}

class Argmax[A](col: Iterable[A]) {
  def argmax[B](f: A => B)(implicit ord: Ordering[B]): Iterable[A] = {
    val mapped = col map f
    val max = mapped max ord
    (mapped zip col) filter (_._1 == max) map (_._2)
  }

  def argmax[B, C](implicit ev: A <:< Tuple2[B, C], ord: Ordering[C]): Iterable[B] = {
    argmax(_._2)(ord) map (_._1)
  }
}

class Sigmoid[A](t: A)(implicit ev: Numeric[A]) {
  def toSigmoid = 1f / (1f + math.exp(-ev.toDouble(t)))
}

object MathOps {

  import scala.collection.generic.CanBuildFrom

  implicit def addArgmax[A](col: Iterable[A]) = new Argmax(col)

  implicit def addSigmoid[A](t: A)(implicit ev: Numeric[A]) = new Sigmoid(t)

  def argmax[A, B](col: Iterable[A])(f: A => B)(implicit ord: Ordering[B]) = {
    new Argmax(col) argmax f
  }

  def sigmoid[A](t: A)(implicit ev: Numeric[A]) = new Sigmoid(t).toSigmoid

  def magnitude[A <% Double](xs: Iterable[A]): Double = {
    math.sqrt(xs.map(x => x * x).sum)
  }

  implicit class Magnitude[A <% Double](xs: Iterable[A]) {
    def magnitude = MathOps.magnitude[A](xs)
  }

  def dotProduct[T <% Double](as: Iterable[T], bs: Iterable[T]) = {
    require(as.size == bs.size)
    (for ((a, b) <- as zip bs) yield a * b).sum
  }

  implicit class DotProduct[T <% Double](s: Iterable[(T, T)]) {
    def dotProduct = s.map(i => i._1 * i._2).sum
  }

  implicit class DotProduct2[T <% Double](s: Iterable[T]) {
    def dot(that: Iterable[T]) = dotProduct(s, that)
  }

  implicit class AddTwoIterables[A, B[A1] <: Iterable[A1]](col: B[A])(implicit ev: Numeric[A], cbf: CanBuildFrom[B[A], A, B[A]]) {
    def +++[B1 <: Iterable[A]](that: B1): B[A] = {
      val ret = cbf()
      ret ++= (col zip that).map(i => ev.plus(i._1, i._2))
      ret.result()
    }
  }

  implicit class RoundOps[A <% Double](value: A) {
    def roundTo(precision: Int): Double = {
      val tmp1 = value * math.pow(10, precision)
      val tmp2 = math.round(tmp1)
      val tmp3 = tmp2 / math.pow(10, precision)
      tmp3
    }
  }

}

object EvalMetrics {
  def precision(tp: Int, fp: Int) = {
    if (tp == 0) 0f
    else tp.toDouble / (tp + fp)
  }

  def recall(tp: Int, fn: Int) = {
    if (tp == 0) 0f
    else tp.toDouble / (tp + fn)
  }

  def accuracy(tp: Int, fp: Int, tn: Int, fn: Int) = {
    (tp.toDouble + tn) / (tp + fp + tn + fn)
  }

  def sensitivity = recall _

  def specificity(tn: Int, fp: Int) = {
    tn.toDouble / (tn + fp)
  }

  def balancedAccuracy(tp: Int, fp: Int, tn: Int, fn: Int) = {
    (specificity(tn, fp) + sensitivity(tp, fn)) / 2
  }

  def fscore(beta: Double)(precision: Double, recall: Double) = {
    (1 + math.pow(beta, 2)) *
      (precision * recall) /
      ((math.pow(beta, 2) * precision) + recall)
  }

  def f1score = fscore(1) _
}