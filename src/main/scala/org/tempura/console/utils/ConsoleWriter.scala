package org.tempura.console.utils

import java.io.PrintStream

object ConsoleCodes {
  // http://linux.about.com/library/cmd/blcmdl4_console_codes.htm
  val ClearLine = "\u001B[K"
  val ClearWholeLine = "\u001B[2K"
  val LineUp = "\u001B[1A"
  val NewLine = "\n"
  val endl = NewLine
}

/**
 * Simple class for writing output over and over to the screen, like progress bars.
 */
class ConsoleWriter()(implicit out: PrintStream = System.out) {

  import ConsoleCodes._
  import org.tempura.utils.CollectionOps._

  val buffer = new StringBuilder

  def clearAll() {
    clear(0)
  }

  def clear(numLines: Int = 0) {
    val nls = buffer.toIterable.indexesWhere(_ == '\n')
    val req = math.abs(numLines)
    if (req > 0 && req < nls.length) {
      val offset = nls(nls.length - (req + 1))
      buffer.delete(offset + 1, buffer.length)
    } else {
      buffer.clear()
    }
    out.print((ClearWholeLine + LineUp) * nls.length)
    out.print(ClearLine)
  }

  private def append(str: String) = {
    buffer.append(str)
    this
  }

  def <<(str: String) = {
    append(str)
    this
  }

  // For Java folks.
  def print(str: String) = <<(str)

  def display() {
    if (!buffer.endsWith("\n")) {
      buffer.append("\n")
    }
    out.print(buffer.toString())
    out.flush()
  }
}

/**
 * Simple class for doing console writing in background.
 * Calls the updateConsole method once every intervalMills milliseconds.
 */
class ConsoleWriterRunnable[T](val intervalMillis: Int = 1000,
                               update: (ConsoleWriter, T) => T,
                               initial: T)
  extends ConsoleWriterRunnableBase {
  var state: T = initial

  def updateConsole(screen: ConsoleWriter) {
    state = update(screen, state)
  }

  def reset() {
    state = initial
  }
}

trait ConsoleWriterRunnableBase extends Runnable {
  val screen = new org.tempura.console.utils.ConsoleWriter

  def intervalMillis: Int

  def updateConsole(screen: ConsoleWriter)

  protected def doRun() {
    screen.clear()
    updateConsole(screen)
    screen.display()
  }

  private var continue = true

  def pause(shouldPause: Boolean = true) {
    continue = !shouldPause
    if (continue) {
      run()
    }
  }

  def run() {
    while (continue) {
      doRun()
      Thread.sleep(intervalMillis)
    }
  }
}


object ConsoleWriterDemo extends App {
  val screen = new ConsoleWriter
  val barSize = 10
  val spins = 50

  def showBootAnimation() {
    for (i <- 0 until spins) {
      screen.clear()
      screen << "Booting ... "
      i % 4 match {
        case 0 => screen << Ansi.Yellow("\\")
        case 1 => screen << Ansi.Blue("|")
        case 2 => screen << Ansi.Red("/")
        case 3 => screen << Ansi.Green("-")
      }
      screen.display()
      Thread.sleep(50)
    }
  }

  def showProgressBar() {
    for (x <- 0 to barSize) {
      screen.clear()
      screen << Ansi.Yellow("[" + ("#" * x) + (" " * (barSize - x)) + "]") << s" $x\n"
      x match {
        case 0                    => screen << "=> Starting up...\n"
        case _ if x > barSize - 1 => screen << "=> Wrapping up..."
        case _                    => screen << "=> Processing...\n"
      }
      screen.display()
      Thread.sleep(500)
    }
    screen.clear(1)
    screen << "Finished."
    screen.display()
  }

  showBootAnimation()
  showProgressBar()
}

/**
 * A second app to show how to easily use the ConsoleWriterRunnable class to
 * print to the screen in a background thread while other processing takes place.
 */
object ConsoleWriterRunnableDemo extends App {
  def showBootAnimation(screen: ConsoleWriter, i: Int) = {
    screen << "Booting ... "
    i % 4 match {
      case 0 => screen << Ansi.Yellow("\\")
      case 1 => screen << Ansi.Blue("|")
      case 2 => screen << Ansi.Red("/")
      case 3 => screen << Ansi.Green("-")
    }
    i + 1
  }
  locally {
    val bgWriter = new ConsoleWriterRunnable(50, showBootAnimation, 0)
    new Thread(bgWriter).start()
    // XXX: Pause the current thread and let the bg console writer keep working
    //      In actuality, this is where we would do all the work we need to do.
    Thread.sleep(10 * 1000)
    bgWriter.pause()
    bgWriter.screen.clear()
    bgWriter.screen.print("Finished.")
    bgWriter.screen.display()
  }
}