package org.tempura.console

package object utils {
  def mkProgressBar(barWidth: Int)(current: Float, total: Float) = {
    val percentDone = ((current + 1) / total * barWidth).toInt
    "[%s%s] (%d/%d)".
      format("#" * percentDone,
        "-" * (barWidth - percentDone),
        current toInt,
        total toInt)
  }

  /**
   * Alternative to using Ansi class directly. Usage:
   *
   * import AnsiStrOps._
   * "Hello!".mkRed
   *
   */
  implicit class AnsiStrOps(str: String) {
    def mkRed = wrapLines(Ansi.Red)(str)
    def mkMagenta = wrapLines(Ansi.Magenta)(str)
    def mkBlue = wrapLines(Ansi.Blue)(str)
    def mkCyan = wrapLines(Ansi.Cyan)(str)
    def mkYellow = wrapLines(Ansi.Yellow)(str)
    def mkGreen = wrapLines(Ansi.Green)(str)
    def mkBold = wrapLines(Ansi.Bold)(str)
    def mkTitle = titlify(str)
    def wrapLines(ansi: Ansi)(str: String) = {
      if (str contains "\n") {
        val lines = for (s <- str split "\n") yield ansi colorize s
        lines mkString "\n"
      } else {
        ansi colorize str
      }
    }
    def titlify(str: String): String = {
      val width = 60
      "=" * width + "\n " + str.capitalize + "\n" + "=" * width
    }
  }
}
