# Utility classes used in various projects of mine #

These are multipurpose utility methods, put in a single place so that I can edit them once and benefit everywhere. Publish to local maven repo with sbt ("sbt publish-local") and then add a dependency to it to include in project:

    libraryDependencies += "org.tempura" %% "common-utils" % "0.1-SNAPSHOT"

## Requirements ##

* Scala 2.10.1 jars in classpath.

## Console Utilities

There are a two useful console output related utility classes in
`org.tempura.console.utils`, `ConsoleWriter`, and `Ansi`, that provide simple
rewriting of the screen (terminal) for things like progress bars, and colourisation of output, respectively.
There are two demo apps, one for Scala, and one for Java, located in the same path demonstrating this. But, briefly:

    def showBootAnimation() {
      val spins = 100
      val screen = new ConsoleWriter
      for (i <- 0 until spins) {
        screen.clear()
        screen << "Booting ... "
        (i % 4) match {
          case 0 => screen << Ansi.Yellow("\\")
          case 1 => screen << Ansi.Blue("|")
          case 2 => screen << Ansi.Red("/")
          case 3 => screen << Ansi.Green("-")
        }
        screen.display()
        Thread.sleep(50)
      }
    }

Will show a spinning line that changes colour.

The same code in Java is:

    public void showBootAnimation() {
      int spins = 100;
      ConsoleWriter screen = new ConsoleWriter(System.out);
      for (int i = 0; i < spins; ++i) {
        screen.clearAll();
        screen.print("Booting ... ");
        switch (i % 4) {
          case 0: 
            screen.print(Ansi.Yellow().format("\\"));
            break;
          case 1: 
            screen.print(Ansi.Blue().format("|"));
            break;
          case 2: 
            screen.print(Ansi.Red().format("/"));
            break;
          case 3: 
            screen.print(Ansi.Green().format("-"));
            break;
        }
        screen.display();
        Thread.sleep(50);
      }
    }